﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly DataContext _dataContext;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var options = DataContextHelper.GetContextOptions();
            DataContextHelper.InitializeDataIfDbIdEmpty(options, (options) => SeedFakeDataFactory(options));
            _dataContext = new DataContext(options);
            _partnerRepository = new EfRepository<Partner>(new DataContext(options));
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Case 1
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromocodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            _partnerRepositoryMock.Setup(a => a.GetByIdAsync(partnerId)).ReturnsAsync(await _partnerRepository.GetByIdAsync(partnerId));

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Case 2
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromocodeLimitAsync_PartnerIsBlocked_ReturnsBadRequest()
        {
            //Arrange
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");
            _partnerRepositoryMock.Setup(a => a.GetByIdAsync(partnerId)).ReturnsAsync(await _partnerRepository.GetByIdAsync(partnerId));
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Case 3, тест необнуляемого числа промокодов
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromocodeLimitAsync_LimitIsExpired_NumberIssuedPromoCodesIsMoreThanZero()
        {
            //Arrange
            var partnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319");
            _partnerRepositoryMock
                .Setup(a => a.GetByIdAsync(partnerId))
                .ReturnsAsync(
                    await _dataContext.Partners
                    .Include(p => p.PartnerLimits)
                    .FirstOrDefaultAsync(p => p.Id == partnerId));

            //Act
            await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId,
                    new SetPartnerPromoCodeLimitRequest
                    {
                        EndDate = DateTime.UtcNow.AddDays(-10),
                        Limit = 10
                    });

            var resultPartner = await _partnerRepository.GetByIdAsync(partnerId);

            //Assert
            resultPartner.NumberIssuedPromoCodes.Should().BeGreaterThan(0);
        }

        /// <summary>
        /// Case 3, тест обнуляемого числа промокодов
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromocodeLimitAsync_LimitIsExpired_NumberIssuedPromoCodesIsZero()
        {
            //Arrange
            var partnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319");
            _partnerRepositoryMock
                .Setup(a => a.GetByIdAsync(partnerId))
                .ReturnsAsync(
                    await _dataContext.Partners
                    .Include(p => p.PartnerLimits)
                    .FirstOrDefaultAsync(p => p.Id == partnerId));

            //Act
            await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId,
                    new SetPartnerPromoCodeLimitRequest
                    {
                        EndDate = DateTime.UtcNow.AddDays(10),
                        Limit = 10
                    });

            var resultPartner = await _partnerRepository.GetByIdAsync(partnerId);

            //Assert
            resultPartner.NumberIssuedPromoCodes.Should().Equals(0);
        }

        /// <summary>
        /// Case 4
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromocodeLimitAsync_InactiveLimitDisabled_ShouldBeTrue()
        {
            //Arrange
            var partnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319");
            _partnerRepositoryMock
                .Setup(a => a.GetByIdAsync(partnerId))
                .ReturnsAsync(
                    await _dataContext.Partners
                    .Include(p => p.PartnerLimits)
                    .FirstOrDefaultAsync(p => p.Id == partnerId));

            //Act
            var expirationDate = DateTime.UtcNow.AddDays(10);
            await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId,
                    new SetPartnerPromoCodeLimitRequest
                    {
                        EndDate = expirationDate,
                        Limit = 10
                    });

            var resultPartner = await _dataContext.Partners
                    .Include(p => p.PartnerLimits)
                    .FirstOrDefaultAsync(p => p.Id == partnerId);
            var activeLimits = resultPartner.PartnerLimits.Where(l => l.CancelDate == null);
            var singleAndAssignedLimit = activeLimits.Count() == 1 && activeLimits.FirstOrDefault().EndDate == expirationDate;

            //Assert
            singleAndAssignedLimit.Should().BeTrue();
        }

        /// <summary>
        /// Case 5
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromocodeLimitAsync_LimitIsGreaterThanZero_ReturnsBadRequest()
        {
            //Arrange
            var partnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319");
            _partnerRepositoryMock.Setup(a => a.GetByIdAsync(partnerId)).ReturnsAsync(
                await _dataContext
                    .Partners
                    .Include(p => p.PartnerLimits)
                    .FirstOrDefaultAsync(p => p.Id == partnerId));
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.UtcNow.AddDays(10),
                Limit = 0
            });

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        /// <summary>
        /// Case 6
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromocodeLimitAsync_LimitIsAddedToDb_ShouldNotBeNull()
        {
            //Arrange
            var partnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319");
            _partnerRepositoryMock
                .Setup(a => a.GetByIdAsync(partnerId))
                .ReturnsAsync(
                    await _dataContext
                        .Partners
                        .Include(p => p.PartnerLimits)
                        .FirstOrDefaultAsync(p => p.Id == partnerId));

            //Act
            var limitEndDate = DateTime.UtcNow.AddDays(10);
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, new SetPartnerPromoCodeLimitRequest
            {
                EndDate = limitEndDate,
                Limit = 10
            });
            var changedPartner = await _dataContext
                .Partners
                .Include(p => p.PartnerLimits)
                .FirstOrDefaultAsync(p => p.Id == partnerId);
            var newLimit = changedPartner.PartnerLimits.FirstOrDefault(l => l.EndDate == limitEndDate);

            //Assert
            newLimit.Should().NotBeNull();
        }

        #region DataHelpers
        private class DataContextHelper
        {
            private static bool DataInitialized = false;
            private static DbContextOptions<DataContext> options = null;

            public static DbContextOptions<DataContext> GetContextOptions()
            {
                if (options == null)
                {
                    options = new DbContextOptionsBuilder<DataContext>()
                        .UseInMemoryDatabase(databaseName: "InMemoryTestDb")
                        .Options;
                }
                return options;
            }

            public static void InitializeDataIfDbIdEmpty(DbContextOptions<DataContext> options, Action<DbContextOptions<DataContext>> seedingCallback)
            {
                if (!DataInitialized)
                {
                    DataInitialized = true;
                    seedingCallback(options);
                }
            }
        }

        private void SeedFakeDataFactory(DbContextOptions<DataContext> options)
        {
            using (var context = new DataContext(options))
            {
                context.Partners.AddRange(FakeDataFactory.Partners);
                context.SaveChanges();
            }
        }

        #endregion
    }
}